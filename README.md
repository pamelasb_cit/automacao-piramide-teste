# Automa��o Pir�mide de testes #

Sugest�o de automa��o para o processo de gera��o da Pir�mide de teste. A solu��o consiste em utilizar o�Google Sheets e Google Forms juntamente com alguns scripts que ser�o executados no pre-push para extra�rem as m�tricas de cada microsservi�o.

### Consolida��o usando Google Sheets ###

Crie uma planilha no Google Sheets onde ficar� consolidada as m�tricas de pir�mide de testes de todos os microsservi�os.

Para cada microsservi�o crie um Google Forms com uma pergunta para cada camada da pir�mide. Exemplo:

* Unit�rio
* Integra��o
* Componente
* E2E
* Explorat�rio

Associe o Forms com a planilha criada anteriormente, isso gerar� uma nova aba na planilha. A cada resposta ser� inserida uma linha na planilha.

![Screenshot](test/resources/images/forms_response.png)

Crie uma f�rmula para pegar sempre a �ltima resposta de cada coluna e salve essa informa��o em outra coluna para este prop�sito. Exemplo:

![Screenshot](test/resources/images/calculo.png)

Crie uma aba para consolidar os resultados de todas as abas (microsservi�os). Exemplo:

![Screenshot](test/resources/images/consolidado.png)

### Automa��o de respostas usando Google Forms ###

Pegue o link compartilh�vel do Forms e fa�a um teste respondendo-o para obter o id dos campos, utilize a fun��o Network do browser. Exemplo:

![Screenshot](test/resources/images/forms_respondido.png)

Para automatizar a resposta do Forms basta enviar um request da seguinte forma:

```sh
curl --location --request POST 'https://docs.google.com/forms/d/.../formResponse' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'entry.id-pergunta-1=resposta pergunta 1' \
--data-urlencode 'entry.id-pergunta-2=resposta pergunta 2' \
--data-urlencode 'entry.id-pergunta-3=resposta pergunta 3'
```

### Automa��o de envio de resposta para o Forms ###

1. Adicione um script que faz um curl para a url do Forms em cada microsservi�o. 

    Exemplo: [test-pyramid-automation.sh](./test-pyramid-automation.sh)

2. Altere o hook de pre-push do git para disparar o script de automa��o como desejar. No exemplo abaixo o script de automa��o da Pir�mide de testes � executado sempre que houver um commit na branch develop.

    Exemplo: [pre-push](./pre-push)

### Automa��o de extra��o do report de testes ###

Crie um script para obter os cen�rios de testes do projeto e salve o total em um arquivo ([test-pyramid-form-response](test/resources/test-pyramid-form-response)) para ser acessado pelo script **test-pyramid-automation.sh**

Exemplo com Spock: [TestReportSpec](test/groovy/report/TestReportSpec.groovy)