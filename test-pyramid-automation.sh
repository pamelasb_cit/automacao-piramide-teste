#!/bin/bash
GOOGLE_FORMS_URL="https://url-google-forms/formResponse"
GOOGLE_FORMS_HEADERS="--header 'Content-Type: application/x-www-form-urlencoded' --header 'Cookie: S=spreadsheet_forms=kZH8m8pRGDYB9d1FeHNqW-CDOP7tJ4U5lXAB7Jc0HTU; NID=222=uI9LtKNGSZEug6j5JuCI_OQOYBwn6n6es_Su2fEjmes8KhFqic1oxqik5lZxKkCfBJi12M_yQIRXN4JZTn41lNctdnRaHPTPkOtxhbjd-Y5s65k7y9EvnxEhWLIQnGr3QcEsQL25i431-mwGhH1jZF1ypLY2j6eiFmFBN7fabpA'"
GOOGLE_FORMS_RESPONSE_FILE="./src/test/resources/test-pyramid-form-response"
GOOGLE_FORMS_RESPONSE=$(cat "$GOOGLE_FORMS_RESPONSE_FILE")
CURL_GOOGLE_FORMS="curl --silent --output /dev/null --location --request POST '${GOOGLE_FORMS_URL}' ${GOOGLE_FORMS_HEADERS} ${GOOGLE_FORMS_RESPONSE}"

# change output color
tput setaf 6

echo "#### Running test pyramid automation ..."
echo "#### Running test report"
./gradlew test --tests=TestReportSpec
# waiting for the report to be updated
sleep 2

tput setaf 6
echo "#### Running google forms"
echo $CURL_GOOGLE_FORMS | bash
echo "#### Finished test pyramid automation!"

# reset output color
tput sgr0
