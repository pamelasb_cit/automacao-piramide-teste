package report


import spock.lang.Specification

class TestReportSpec extends Specification {

    private static final String GOOGLE_FORM_RESPONSE_PATH = "./src/test/resources/test-pyramid-form-response"
    private static final String GOOGLE_FORM_RESPONSE = "--data-urlencode 'entry.663751502=%s' --data-urlencode 'entry.627028838=%s' --data-urlencode 'entry.1961621878=%s'"
    static def totalUnit
    static def totalIntegration
    static def totalComponent

    def "executa o report de testes"() {
        given: "que execute os testes"
        println "Running Test Report"

        when: "ao executar a extra��o do report de testes"
        totalUnit = runTestReport("./build/reports/tests/test/index.html")
        totalIntegration = runTestReport("./build/reports/tests/integrationTest/index.html")
        totalComponent = runTestReport("./build/reports/tests/componentTest/index.html")

        and: "atualizar a resposta do google forms"
        updateTestPyramidFormResponse()

        then: "ser� gerado a sa�da"
        println("Finish Test Report")
    }

    private static runTestReport(def path) {
        File htmlReport = new File(path)
        getTotalTestsFromFile(htmlReport)
    }

    private static updateTestPyramidFormResponse() {
        def googleFormsResponse = String.format(GOOGLE_FORM_RESPONSE, totalUnit, totalIntegration, totalComponent)

        File googleFormsResponsefile = new File(GOOGLE_FORM_RESPONSE_PATH)
        googleFormsResponsefile.write(googleFormsResponse)
    }

    private static String getTotalTestsFromFile(File f) {
        try {
            Scanner scanner = new Scanner(f)
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine()
                if (isDivInfoBoxLine(line)) {
                    String nextLine = scanner.nextLine()
                    return getTotalTestsFromLine(nextLine)
                }
            }
        } catch (Exception e) {
            println("Erro ao executar o report de teste do arquivo: ${f.toString()}} : ${e}")
        }
        "default value"
    }

    private static String getTotalTestsFromLine(String line) {
        line.substring(line.indexOf(">") + 1, line.lastIndexOf("<"))
    }

    private static boolean isDivInfoBoxLine(String line) {
        line.contains("infoBox")
    }
}